function allData(){
            
    table.innerHTML = ``
    contactList = JSON.parse(localStorage.getItem('listItem')) ?? []

    contactList.forEach(function (value, i){
        
        var table = document.getElementById('table')

        table.innerHTML += `
            
            <tr>
                <td >${i+1}.</td>
                <td class="checked">${value.name}</td>
                <td>${value.sum}</td>
                <td>
                    <button class="btn btn-sm btn-info" onclick="find(${value.id})">
                        <i class="fa-solid fa-file-pen"></i>
                    </button>
                </td>

                <td>
                    <button class="btn btn-sm btn-warning" onclick="removeData(${value.id})">
                    <i class="fa-solid fa-delete-left"></i>
                    </button>
                </td>
            </tr>`
    })

    var list = document.querySelector('td');
    list.addEventListener('click', function(ev) {
    if (ev.target.tagName === 'table') {
        ev.target.classList.toggle('checked');
    }
    }, false);
}